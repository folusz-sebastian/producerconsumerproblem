import java.util.LinkedList;

import static java.lang.Thread.sleep;

public class Producer implements Runnable{
    private LinkedList<Integer> list;
    private int capacity;
    private static int counter = 0;
    private int idProducer;
    private String name;
    private Monitor warehouse;

    public Producer(LinkedList<Integer> list, int capacity, Monitor warehouse) {
        this.warehouse = warehouse;
        counter ++;
        idProducer = counter;
        name = "producer"+idProducer;
        this.capacity = capacity;
        this.list = list;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public void run() {
        while (true){
            synchronized (warehouse){
                while (list.size()==capacity){
                    try {
                        warehouse.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                warehouse.produce();

                warehouse.notifyAll();

                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
