import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        final int capacity = 4;
        final int noOfConsumers = 3;
        Monitor warehouse = new Monitor(list);

        Consumer consumer = new Consumer(list, warehouse);
        Producer producer = new Producer(list, capacity, warehouse);

        new Thread(producer).start();
        for (int i = 0; i < noOfConsumers; i++) {
            new Thread(consumer).start();
        }
    }
}
