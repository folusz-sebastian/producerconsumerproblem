import java.util.LinkedList;

import static java.lang.Thread.sleep;

public class Consumer implements Runnable{
    private LinkedList<Integer> list;
    private static int counter = 0;
    private int idConsumer;
    private String name;
    private Monitor warehouse;

    public Consumer(LinkedList<Integer> list, Monitor warehouse) {
        this.warehouse = warehouse;
        counter++;
        idConsumer = counter;
        name = "consumer" + idConsumer;
        this.list = list;
    }

    @Override
    public String toString() {
        return "consumer" + idConsumer;
    }

    @Override
    public void run() {
        while (true){
            synchronized (warehouse){
                while (list.size()==0) {
                    try {
                        warehouse.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                warehouse.consume();

                warehouse.notifyAll();

                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
