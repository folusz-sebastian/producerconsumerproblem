import java.util.LinkedList;

public class Monitor {
    private LinkedList<Integer> list;
    private int element;

    public Monitor(LinkedList<Integer> list) {
        this.list = list;
        element = 0;
    }

    void produce (){
        System.out.println("produkuje: "  + element);
        element++;
        list.add(element);
    }

    void consume(){
        System.out.println("konsumuje: " + list.removeFirst());
    }
}
